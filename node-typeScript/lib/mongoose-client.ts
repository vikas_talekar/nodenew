import {Document, model, Model, Schema} from 'mongoose';

var UserSchema: Schema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  age: Number,
  friends: [String],
  data: [Schema.Types.Mixed]
});

interface IUser extends Document {
  username: string;
  age: number;
  friends: string[];
  data: any[];
}

var UserModel: Model<IUser> = model<IUser>('User', UserSchema);

var user = new UserModel({name: 'Jane'});
user.username;     // IUser properties are available
user.save();       // mongoose Document methods are available

UserModel.findOne({}, (err: any, user: IUser) => {
  user.username;   // IUser properties are available
  user.save();     // mongoose Document methods are available
});

export { UserSchema};