// server.js
const express = require("express");
const redisClient = require("./redis-client");
const app = express();
const mongodb = require('mongodb');
const config = require('./mongo-client');

const client = mongodb.MongoClient;

client.connect(config.DB, function(err, db) {
    if(err) {
        console.error(err);
        console.log('database is not connected');
    }
    else {
        console.log('connected!!');
        var dbo = db.db("docker");
        // dbo.createCollection('server',{},function(err,result){
        //     if (err) throw err;
        //     console.log("created");
        // });
        dbo.collection("server").findOne({},function(err, result) {
            if (err) throw err;
            console.log(result);
            db.close();
        });
    }
});

app.get("/", (req, res) => {
  return res.send("Hello world");
});

app.get("/store/:key", async (req, res) => {
  const { key } = req.params;
  const value = req.query;
  const s = await redisClient.setAsync(key, JSON.stringify(value));
  return res.send("Success" + s);
});
app.get("/:key", async (req, res) => {
  const { key } = req.params;
  const rawData = await redisClient.getAsync(key);
  return res.json(JSON.parse(rawData));
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
 
